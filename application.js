//the variable that will accommodate the WebGL context
//every call to the state machine will be done through this variable
var gl;
var canvas;

//Initialize WebGL
function initGL(canvas) {
  //get a webgl context
  gl = canvas.getContext("webgl", {preserveDrawingBuffer: true}) || canvas.getContext("experimental-webgl");

  if (!gl) {
    alert("Could not initialize WebGL, sorry :-(");
  }    
  
  //assign a viewport width and height based on the HTML canvas element properties
  gl.viewportWidth = canvas.width;
  gl.viewportHeight = canvas.height;

  resizeCanvas();
}


//Find and compile shaders (vertex + fragment shader)
function getShader(gl, id) {
  //gets the shader scripts (vertex + fragment)
  var shaderScript = document.getElementById(id);
  if (!shaderScript) {
    return null;
  }

  var str = "";
  var k = shaderScript.firstChild;
  while (k) {
    if (k.nodeType == 3) {
      str += k.textContent;
    }
    k = k.nextSibling;
  }

  var shader;
  // create shaders
  if (shaderScript.type == "x-shader/x-fragment") {
    shader = gl.createShader(gl.FRAGMENT_SHADER);
  } else if (shaderScript.type == "x-shader/x-vertex") {
    shader = gl.createShader(gl.VERTEX_SHADER);
  } else {
    return null;
  }

  // ask WebGL to compile shaders
  gl.shaderSource(shader, str);
  gl.compileShader(shader);

  // check for errors in the shaders
  if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
    alert(gl.getShaderInfoLog(shader));
    return null;
  }

  return shader;
}


var shaderProgram;

//Creates a program from a vertex + fragment shader pair
function initShaders() {
  var fragmentShader = getShader(gl, "shader-fs");
  var vertexShader = getShader(gl, "shader-vs");

  shaderProgram = gl.createProgram();
  gl.attachShader(shaderProgram, vertexShader);
  gl.attachShader(shaderProgram, fragmentShader);
  
  //link the compiled binaries
  gl.linkProgram(shaderProgram);

  //check for errors, again
  if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
      alert("Could not initialise shaders");
  }

  //activate current program
  //this sandbox has only on shader pair
  //we can have as many as we wish in more complex applications
  gl.useProgram(shaderProgram);

  //Update attributes for the vertex shader
  //attributes are accessible only from the vertex shader
  //if we want accessible data from a fragment shader we can use uniform variables,
  //or varyings that will be forwarded from the vertex shader to the fragment shader

  //Vertex position data
  shaderProgram.vertexPositionAttribute = gl.getAttribLocation(shaderProgram, "aVertexPosition");
  gl.enableVertexAttribArray(shaderProgram.vertexPositionAttribute);

  //Vertex texture data
  shaderProgram.textureCoordAttribute = gl.getAttribLocation(shaderProgram, "aTextureCoord");
  gl.enableVertexAttribArray(shaderProgram.textureCoordAttribute);

  shaderProgram.vertexNormalAttribute = gl.getAttribLocation(shaderProgram, "aVertexNormal");
  gl.enableVertexAttribArray(shaderProgram.vertexNormalAttribute);
  // shaderProgram.textureCoordAttribute = gl.getAttribLocation(shaderProgram, "aTextureCoord");
  // gl.enableVertexAttribArray(shaderProgram.textureCoordAttribute);

  //Update uniform variables
  //this variables can be accessed from both the vertex and fragment shader
  shaderProgram.pMatrixUniform = gl.getUniformLocation(shaderProgram, "uPMatrix");
  shaderProgram.mvMatrixUniform = gl.getUniformLocation(shaderProgram, "uMVMatrix");
  shaderProgram.nMatrixUniform = gl.getUniformLocation(shaderProgram, "uNMatrix");
  shaderProgram.samplerUniform = gl.getUniformLocation(shaderProgram, "uSampler");
  shaderProgram.useLightingUniform = gl.getUniformLocation(shaderProgram, "uUseLighting");
  shaderProgram.ambientColorUniform = gl.getUniformLocation(shaderProgram, "uAmbientColor");
  shaderProgram.pointLightingLocationUniform = gl.getUniformLocation(shaderProgram, "uPointLightingLocation");
  shaderProgram.pointLightingColorUniform = gl.getUniformLocation(shaderProgram, "uPointLightingColor");
}

// holding the textures, handling their initialization and drawing
var textures = new TextureManager();

function initTextures() {
  textures.setTexture("moon", "basketball2.jpg");
  textures.setTexture("crate", "crate.jpg");
  textures.setTexture("finalCrate", "crate-final.jpg");
  textures.setTexture("sea", "grass2.jpg");
  textures.setTexture("sky", "gras_bg.jpg");
}

//ModelView and Projection matrices
//mat4 comes from the external library
var mvMatrix = mat4.create();
var mvMatrixStack = [];
var pMatrix = mat4.create();

//The matrix stack operation are implemented below to handle local transformations

//Push Matrix Operation
function mvPushMatrix() {
  var copy = mat4.create();
  mat4.set(mvMatrix, copy);
  mvMatrixStack.push(copy);
}

//Pop Matrix Operation
function mvPopMatrix() {
  if (mvMatrixStack.length == 0) {
      throw "Invalid popMatrix!";
  }
  mvMatrix = mvMatrixStack.pop();
}


//Sets + Updates matrix uniforms
function setMatrixUniforms() {
  gl.uniformMatrix4fv(shaderProgram.pMatrixUniform, false, pMatrix);
  gl.uniformMatrix4fv(shaderProgram.mvMatrixUniform, false, mvMatrix);

  var normalMatrix = mat3.create();
  mat4.toInverseMat3(mvMatrix, normalMatrix);
  mat3.transpose(normalMatrix);
  gl.uniformMatrix3fv(shaderProgram.nMatrixUniform, false, normalMatrix);
}


//Rotation function helper
function degToRad(degrees) {
  return degrees * Math.PI / 180;
}


var scene;
var cube;
var square;
var sphere;
var pyramid;
var game;

var tileDistHor = 2.3;
var tileDistVer= 2;
var sphereDistance = 1.8;


    
//Initialize VBOs, IBOs and color
function initBuffers() {

  game = new BlockDropGame(10,10);
  game.generateCanvas();


  scene = new Scene(gl.viewportWidth, gl.viewportHeight);

  cube = new Scene3dObject();

  cube.vertices = [

    // Front face
    -1.0, -1.0,  1.0,
    1.0, -1.0,  1.0,
    1.0,  1.0,  1.0,
    -1.0,  1.0,  1.0,

    // Back face
    -1.0, -1.0, -1.0,
    -1.0,  1.0, -1.0,
    1.0,  1.0, -1.0,
    1.0, -1.0, -1.0,

    // Top face
    -1.0,  1.0, -1.0,
    -1.0,  1.0,  1.0,
    1.0,  1.0,  1.0,
    1.0,  1.0, -1.0,

    // Bottom face
    -1.0, -1.0, -1.0,
    1.0, -1.0, -1.0,
    1.0, -1.0,  1.0,
    -1.0, -1.0,  1.0,

    // Right face
    1.0, -1.0, -1.0,
    1.0,  1.0, -1.0,
    1.0,  1.0,  1.0,
    1.0, -1.0,  1.0,

    // Left face
    -1.0, -1.0, -1.0,
    -1.0, -1.0,  1.0,
    -1.0,  1.0,  1.0,
    -1.0,  1.0, -1.0
  ];

  cube.normals = [
    // Front face
    0.0,  0.0,  1.0,
    0.0,  0.0,  1.0,
    0.0,  0.0,  1.0,
    0.0,  0.0,  1.0,

    // Back face
    0.0,  0.0, -1.0,
    0.0,  0.0, -1.0,
    0.0,  0.0, -1.0,
    0.0,  0.0, -1.0,

    // Top face
    0.0,  1.0,  0.0,
    0.0,  1.0,  0.0,
    0.0,  1.0,  0.0,
    0.0,  1.0,  0.0,

    // Bottom face
    0.0, -1.0,  0.0,
    0.0, -1.0,  0.0,
    0.0, -1.0,  0.0,
    0.0, -1.0,  0.0,

    // Right face
    1.0,  0.0,  0.0,
    1.0,  0.0,  0.0,
    1.0,  0.0,  0.0,
    1.0,  0.0,  0.0,

    // Left face
    -1.0,  0.0,  0.0,
    -1.0,  0.0,  0.0,
    -1.0,  0.0,  0.0,
    -1.0,  0.0,  0.0,
  ];

  cube.texture = [
      // Front face
      0.0, 0.0,
      1.0, 0.0,
      1.0, 1.0,
      0.0, 1.0,

      // Back face
      1.0, 0.0,
      1.0, 1.0,
      0.0, 1.0,
      0.0, 0.0,

      // Top face
      0.0, 1.0,
      0.0, 0.0,
      1.0, 0.0,
      1.0, 1.0,

      // Bottom face
      1.0, 1.0,
      0.0, 1.0,
      0.0, 0.0,
      1.0, 0.0,

      // Right face
      1.0, 0.0,
      1.0, 1.0,
      0.0, 1.0,
      0.0, 0.0,

      // Left face
      0.0, 0.0,
      1.0, 0.0,
      1.0, 1.0,
      0.0, 1.0,
  ];

  cube.vertexIndices = [
    //this numbers are positions in the VBO array above
    0, 1, 2,      0, 2, 3,    // Front face
    4, 5, 6,      4, 6, 7,    // Back face
    8, 9, 10,     8, 10, 11,  // Top face
    12, 13, 14,   12, 14, 15, // Bottom face
    16, 17, 18,   16, 18, 19, // Right face
    20, 21, 22,   20, 22, 23  // Left face
  ];


  sphere = new Scene3dObject();

  var latitudeBands = 30;
  var longitudeBands = 30;
  var radius = 2;

  var vertexPositionData = [];
  var normalData = [];
  var textureCoordData = [];
  for (var latNumber=0; latNumber <= latitudeBands; latNumber++) {
    var theta = latNumber * Math.PI / latitudeBands;
    var sinTheta = Math.sin(theta);
    var cosTheta = Math.cos(theta);

    for (var longNumber=0; longNumber <= longitudeBands; longNumber++) {
      var phi = longNumber * 2 * Math.PI / longitudeBands;
      var sinPhi = Math.sin(phi);
      var cosPhi = Math.cos(phi);

      var x = cosPhi * sinTheta;
      var y = cosTheta;
      var z = sinPhi * sinTheta;
      var u = 1 - (longNumber / longitudeBands);
      var v = 1 - (latNumber / latitudeBands);

      normalData.push(x);
      normalData.push(y);
      normalData.push(z);
      textureCoordData.push(u);
      textureCoordData.push(v);
      vertexPositionData.push(radius * x);
      vertexPositionData.push(radius * y);
      vertexPositionData.push(radius * z);
    }
  }

  var indexData = [];
  for (var latNumber=0; latNumber < latitudeBands; latNumber++) {
    for (var longNumber=0; longNumber < longitudeBands; longNumber++) {
      var first = (latNumber * (longitudeBands + 1)) + longNumber;
      var second = first + longitudeBands + 1;
      indexData.push(first);
      indexData.push(second);
      indexData.push(first + 1);

      indexData.push(second);
      indexData.push(second + 1);
      indexData.push(first + 1);
    }
  }

  sphere.vertices = vertexPositionData;
  sphere.normals = normalData;
  sphere.texture = textureCoordData;
  sphere.vertexIndices = indexData;



  floor = new Scene2dObject();

  // floor
  floor.vertices = [
    -1.0,  0.0,  -1.0,
    1.0,  0.0,  -1.0,
    -1.0,  0.0,   1.0,
    1.0,  0.0,   1.0
  ];

  floor.texture = [
    0.0, 0.0,
    0.0, 1.0,
    1.0, 0.0,
    1.0, 1.0,
  ];
}

//Helper Variables
var rCube = 0;
var xTrans = 0.0;
var yTrans = 0.0;

var pitch = 15;
var pitchRate = 0;

var yaw = 0;
var yawRate = 0;

var xPos = -2;
var yPos = 1.0;
var zPos = 4.0;

var speed = 0;
// - Mouse
/////////////////////////

var mouseDown = false;
var lastMouseX = null;
var lastMouseY = null;

var cameraRotationMatrix = mat4.create();
mat4.identity(cameraRotationMatrix);

function handleMouseDown(event) {
  mouseDown = true;
  lastMouseX = event.clientX;
  lastMouseY = event.clientY;
}

function handleMouseUp(event) {
  mouseDown = false;
}


function handleMouseMove(event) {
  if (!mouseDown) {
    return;
  }
  var newX = event.clientX;
  var newY = event.clientY;

  var deltaX = newX - lastMouseX
  var newRotationMatrix = mat4.create();
  mat4.identity(newRotationMatrix);
  mat4.rotate(newRotationMatrix, degToRad(deltaX / 10), [0, 1, 0]);

  var deltaY = newY - lastMouseY;
  mat4.rotate(newRotationMatrix, degToRad(deltaY / 10), [1, 0, 0]);

  mat4.multiply(newRotationMatrix, cameraRotationMatrix, cameraRotationMatrix);

  lastMouseX = newX
  lastMouseY = newY;
}

// - Keyboard
/////////////////////////
//array for keeping pressed keys
var currentlyPressedKeys = {};
var command;

//Keyboard handler
var click = false;

function handleKeyDown(event) {
  currentlyPressedKeys[event.keyCode] = true;

  if(command == null) {
    // if(event.keyCode == 16)
  }
}

function unclick() {
  click = false;
}

//Keyboard handler
//do not touch :) 
function handleKeyUp(event) {
  currentlyPressedKeys[event.keyCode] = false;
}

//Key pressed callback
//37-40 are the codes for the arrow keys
//xTrans + yTrans are used in the ModelView matrix for local transformation of the cube
function handleKeys() {

  // [Ctrl - H] keybinding
  // starts help mode which shows the next move on the minimap
  if (currentlyPressedKeys[72] && currentlyPressedKeys[18]) {
    
    if(helpMe == true) {
      helpMe = false;
    }  else {
      helpMe = true;
    }

    drawMiniMap();
    currentlyPressedKeys[72] = false;
  }

  if (currentlyPressedKeys[87]) {
    // [W] keybinding
    pitchRate = 0.1;
  } else if (currentlyPressedKeys[83]) {
    // [S] keybinding
    pitchRate = -0.1;
  } else {
    pitchRate = 0;
  }


  if(click) {
    return;
  }

  //click = true;
  step = 1;
  if(currentlyPressedKeys[16]) {
    step = 2;
  }

  if (currentlyPressedKeys[37]) {
    // Left cursor key
    movePlayer({left:step});
    currentlyPressedKeys[37] = false;
  }
  if (currentlyPressedKeys[39]) {
    // Right cursor key
    movePlayer({right:step});
    currentlyPressedKeys[39] = false;
  }
  if (currentlyPressedKeys[38]) {
    // Up cursor key
    movePlayer({forward:step});
    currentlyPressedKeys[38] = false;
  }
  if (currentlyPressedKeys[40]) {
    // Down cursor key
    movePlayer({back:step});
    currentlyPressedKeys[40] = false;
  }
}


function movePlayer(object) {
  droppingTile = {
    x: game.playerPosition.x,
    y: game.playerPosition.y
  };

  click = true;

  droppingTile.height = 0;

  //hack, needs replacing
  var ox = game.playerPosition.x;
  var oy = game.playerPosition.y;
  parabolicMove.start({x:ox,y:oy}, game.movePlayer(object));
  setTimeout(function(){
    parabolicMove.stop();
    checkPlayStatus();
    drawMiniMap();
    unclick();
  }, parabolicDuration);
}

function checkPlayStatus() {
  if(game.hasPlayerLost()) {
    unbindKeyboard();
    $('#lostBox').show();
    setTimeout(function(){
      $('#lostBox').fadeOut();
    },5000);
    setTimeout(function(){
      game.resetLevel();
      drawMiniMap();
      bindKeyboard();
    },1000);
  }

  if(game.hasPlayerWon()) {
    $('#wonBox').show();
    unbindKeyboard();
    setTimeout(function(){
      $('#wonBox').fadeOut();
    },5000);

    setTimeout(function(){
      game.nextLevel();
      $('#level').text("level " + game.levelNum);
      drawMiniMap();
      bindKeyboard();
    },1500);
  }
}


var tileDropDuration = 1000;
var droppingTile = null;

function dropTile(elapsed) {
  if(droppingTile == null) {
    return;
  }

  droppingTile.height -= elapsed/tileDropDuration * tileDistVer;
  
  if(droppingTile.height + tileDistVer <= 0) {
    droppingTile = null;
  }
}

var middleHorOffset = tileDistVer;
var parabolicDuration = 1000;

parabolicMove = {
  x: null,
  y: null,
  z: null,

  startX: 0,
  startY: 0,
  startZ: 0,

  endX: 0,
  endY: 0,
  endZ: 0,

  midX: 0,
  midY: 0,
  midZ: 0,

  totalElapsed: 0,
  upWards: true,
  prevGain: 0,

  // BUG: does not go underwater when player loses
  // BUG: does not work when user goes off the map
  // TODO: can be tweaked a lot
  start: function(startPosition, endPosition) {
    this.startX = -(startPosition.x * tileDistHor);
    this.startZ = -(startPosition.y * tileDistHor);
    this.startY = tileDistVer * (game.getItem(startPosition.x, startPosition.y).height) + sphereDistance;

    this.endX = -(endPosition.x * tileDistHor);
    this.endZ = -(endPosition.y * tileDistHor);
    this.endY = tileDistVer * (game.getItem(endPosition.x, endPosition.y).height-1) + sphereDistance;

    this.midX = (this.endX + this.startX) / 2;
    this.midZ = (this.endZ + this.startZ) / 2;

    // console.log(endPosition);
    // console.log(startPosition);
    if(Math.abs(endPosition.x + endPosition.y - startPosition.x - startPosition.y) == 1) {
        // moving is a single step, so lower the middle height
        this.midY = Math.max(this.endY, this.startY) + middleHorOffset/2;
    } else {
        this.midY = Math.max(this.endY, this.startY) + middleHorOffset;
    }

    this.x = this.startX;
    this.y = this.startY;
    this.z = this.startZ;
  },

  isActive: function() {
    return this.x != null;
  },

  stop: function() {
    this.x = null;
    this.y = null;
    this.z = null;

    this.totalElapsed = 0;
    this.upWards = true;
  },

  move: function(elapsed) {
    if(!this.isActive()) {
      return;
    }

    this.totalElapsed += elapsed;
    
    var operc = elapsed / parabolicDuration*2;
    
    if(this.totalElapsed < parabolicDuration/2) {
        //not sure if this works as it is supposed to
        totalPerc = this.totalElapsed / (parabolicDuration / 2);

        gained = totalPerc + (1 - totalPerc) * 0.20;
        perc = gained;

        percy = perc + totalPerc;

        this.z = this.startZ + perc * (this.midZ - this.startZ);
        this.x = this.startX + perc * (this.midX - this.startX);
        this.y = this.startY + perc * (this.midY - this.startY);
        this.y = this.startY + perc * (this.midY - this.startY);

        // console.log('mid-start: ' + (this.midY - this.startY) + ' - perc: '+ perc+' - up ' + this.y);
    } else {
        totalPerc = (this.totalElapsed - (parabolicDuration / 2)) / (parabolicDuration / 2);
        if(totalPerc > 1) {
            totalPerc = 1;
        }
        gained = totalPerc + (1 - totalPerc) * 0.20;

        perc = gained;

        this.z = this.midZ + perc * (this.endZ - this.midZ);
        this.x = this.midX + perc * (this.endX - this.midX);
        this.y = this.midY + perc * (this.endY - this.midY);
        // this.y = this.midY + perc * (this.endY - this.midY) + 0.50 * (1 - totalPerc);

        // console.log('end-mid: ' + (this.endY - this.midY) + ' - perc: '+ perc+' - down: ' + this.y);
        // this.y = this.midY + perc * (this.endY - this.midY) + 0.25 * (1 - totalPerc);
    }
  }
}

    
  //For every frame this function draws the complete scene from the beginning
function drawScene() {
  //the viewport gets the canvas values (that were assigned to the gl context variable)
  gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);

  //the frame and depth buffers get cleaned (the depth buffer is used for sorting fragments)
  //without the depth buffer WebGL does not know which fragment is visible for a given pixel
  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

  //the projection matrix (pMatrix) is set
  //45 degrees Field-Of-View
  //aspect ratio gl.viewportWidth / gl.viewportHeight
  //near plane: 0.1 , far plane: 100
  mat4.perspective(60, gl.canvas.width / gl.canvas.height, 0.1, 100.0, pMatrix);


  //use lighting
  gl.uniform1i(shaderProgram.useLightingUniform, false);

  //the modelview Matrix is initialized with the Identity Matrix
  mat4.identity(mvMatrix);

  //the ModelView matrix gets a global transformation ("camera" retracts 8 units)
  //otherwise the "camera" will be inside the rotating cube
  //z-axis points out of the screen. we translate -8 which is the inverse transform
  //in essence we move the world -8 units to have the camera 8 units forward.
  //REMEMBER there is no actual camera in WebGL

  mat4.translate(mvMatrix, [-xPos, -yPos, -zPos]);
  mat4.rotate(mvMatrix, degToRad(pitch), [1, 0, 0]);
  mat4.rotate(mvMatrix, degToRad(-yaw), [0, 1, 0]);

  mat4.scale(mvMatrix, [0.2, 0.2, 0.2]);
  mat4.multiply(mvMatrix, cameraRotationMatrix);

  mvPushMatrix();

  // set the texture for the cubes
  textures.crate.set();

  mvPushMatrix();

  for(var i=0; i < game.sizeY; i++) {

    mvPushMatrix();
    for(var j=0; j < game.sizeX; j++) {
      var tile = game.getItem(i,j);
      mvPushMatrix();

      // if the tile is dropping, draw the bottom in place
      if(droppingTile!= null && droppingTile.x == i && droppingTile.y == j) {
        mat4.translate(mvMatrix, [0.0, droppingTile.height, 0.0]);
        tile.height++;
      }

      // draw the tiles
      for(var k=0; k < tile.height-1; k++) {
        num = Math.random();
        cube.draw();
        mat4.translate(mvMatrix, [0.0, tileDistVer, 0.0]);
      }

      if(tile.height > 0) {
        if(tile.isEnd) {

          textures.finalCrate.set();
          
          cube.draw();

          textures.crate.set();
        } else{
          cube.draw();
        }
      }

      mvPopMatrix();
      mat4.translate(mvMatrix, [0.0, 0.0, -tileDistHor]);
    }
    mvPopMatrix();

    mat4.translate(mvMatrix, [-tileDistHor, 0.0, 0.0]);
  }
  mvPopMatrix();

  // Draw the image on the back
  mvPushMatrix();

  // check if the sphere is on the move the matrix to the right spot
  if(parabolicMove.isActive()) {
    mat4.translate(mvMatrix, [parabolicMove.x, parabolicMove.y, parabolicMove.z]);
  } else {
    var position = game.getPlayerPosition();
    var x = -(position.x * tileDistHor);
    var z = -(position.y * tileDistHor);
    var y = tileDistVer * (game.getItem(position.x, position.y).height - 1) + sphereDistance;
    mat4.translate(mvMatrix, [x, y, z]);
  }

  mat4.scale(mvMatrix, [0.4, 0.4, 0.4]);

  // Set the texture and draw the sphere
  textures.moon.set();
  sphere.draw();

  mvPopMatrix();

  // Draw the image on the bottom
  mvPushMatrix();

  // Move/stretch the matrix to set in position
  mat4.translate(mvMatrix, [0, -1.0, 15.0]);
  mat4.rotate(mvMatrix, degToRad(90), [0, 1, 0]);
  mat4.scale(mvMatrix, [100, 1, 50]);

  // Set the texture and draw
  textures.sea.set();
  floor.draw();

  mvPopMatrix();

  // Draw the image on the back
  mvPushMatrix();

  // Move/stretch the matrix to set in position
  mat4.translate(mvMatrix, [0, 0, -40.0]);
  mat4.rotate(mvMatrix, degToRad(90), [1, 0, 0]);
  mat4.rotate(mvMatrix, degToRad(90), [0, 1, 0]);
  mat4.rotate(mvMatrix, degToRad(-20), [0, 0, 1]);
  mat4.scale(mvMatrix, [20, 10, 35]);
  mat4.translate(mvMatrix, [0.85, 1, -0.3]);

  // Set the texture and draw
  textures.sky.set();
  floor.draw();

  mvPopMatrix();
}

//animation parameter
var lastTime = 0;
var minPitch = 15;
var maxPitch = 80;

function animate() {
  var timeNow = new Date().getTime();
  if (lastTime != 0) {
    var elapsed = timeNow - lastTime;

    if (speed != 0) {
      xPos -= Math.sin(degToRad(yaw)) * speed * elapsed;
      zPos -= Math.cos(degToRad(yaw)) * speed * elapsed;

      joggingAngle += elapsed * 0.6; // 0.6 "fiddle factor" - makes it feel more realistic :-)
      yPos = Math.sin(degToRad(joggingAngle)) / 20 + 0.4
    }

    //yaw += yawRate * elapsed;
    pitch += pitchRate * elapsed;

    if(pitch > maxPitch) {
      pitch = maxPitch;
    }

    if(pitch < minPitch) {
      pitch = minPitch;
    }

    // set y position relative to the pitch, moving the board as the user changes the angle.
    yPos = 1 + (pitch - minPitch) / (maxPitch - minPitch);
  }
  
  dropTile(elapsed);

  parabolicMove.move(elapsed);

  lastTime = timeNow;
}


//this is the requestAnimFrame callback
//For every tick, request another frame
//handle keyboard, draw the scene, animate (update animation variebles) and continue
function tick() {
  requestAnimFrame(tick);
  handleKeys();
  drawScene();
  animate();
}

var helpMe = false;

//Entry point of the WebGL context
function webGLStart() {
  var canvas = document.getElementById("TUCWebGL");
    
  //Functions for initialization
  //Check above
  initGL(canvas);
  initShaders();
  initBuffers();
  initTextures();

  bindUserInput(canvas);

  //Background Color: Color assigned for all pixels with no corresponding fragments
  //gl.clearColor(0.5, 0.5, 0.5, 1.0);

  //Enable z-buffer for depth sorting
  gl.enable(gl.DEPTH_TEST);

  //the first tick of our application
  tick();
}

// Attaches the handlers for the keyboard from the document
function bindKeyboard() {
  $(document).on("keydown", handleKeyDown);
  $(document).on("keyup", handleKeyUp);
}

// Dettaches the handlers for the keyboard from the document
function unbindKeyboard() {
  // we need to remove the arrow keys from the array,
  // in case the user holds a key down while loosing.
  currentlyPressedKeys[37] = false;
  currentlyPressedKeys[39] = false;
  currentlyPressedKeys[38] = false;
  currentlyPressedKeys[40] = false;

  // remove the handlers
  $(document).off("keydown", handleKeyDown);
  $(document).off("keyup", handleKeyUp);
}

// Initializes the handlers for all the user input.
function bindUserInput(canvas) {
  
  // keyoards handlers
  bindKeyboard();

  // window resize handler
  window.addEventListener('resize', resizeCanvas);

  // setup the level
  $('#level').text("level " + game.levelNum);

  // handle the replay button
  $("#replayBtn").click(function(){
    game.resetLevel();
    $('#gameOverModal').modal('hide');
    drawMiniMap();
  });

  // handle the level reset button
  $("#resetLevelBtn").click(function(){
    game.resetLevel();
    drawMiniMap();
  });

  // handle the next level button
  $("#nextLevelBtn").click(function(){
    game.nextLevel();
    $('#level').text("level " + game.levelNum);
    drawMiniMap();
  });

  // handle the goto level button
  $("#gotoLvl").click(function(){
    var lvl = parseInt($("#gotoLvlInput").val());

    // go up to level 200
    if(lvl > 0 && lvl < 200) {
      game.gotoLevel(lvl);
      $('#level').text("level " + game.levelNum);
      drawMiniMap();
    }
  });
  
  $(function() {
    $("form input").keypress(function (e) {
      if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
        $('button[type=submit] .default').click();
        return false;
      } else {
        return true;
      }
    });
  });
}

// resize the canvas and fit it to it's parent div
// TODO: move this to Scene object
function resizeCanvas() {
  sidebar = $(".sidebar");
  footer = $("#footer");
  canvasDiv = $("#canvasesdiv");

  canvasDiv.height(sidebar.height() - footer.height());

  $("#TUCWebGL").width(canvasDiv.width());
  $("#TUCWebGL").height(canvasDiv.height());

  gl.canvas.width = $("#TUCWebGL").width();
  gl.canvas.height = $("#TUCWebGL").height();
}