function BlockDropGame(canvasSizeX, canvasSizeY) {
  this.sizeX = canvasSizeX;
  this.sizeY = canvasSizeY;
}

BlockDropGame.prototype = {
  sizeX: 0,
  sizeY: 0,

  canvas: null,

  levelNum: 1,

  level: null,

  tiles: [],

  solution : new LevelSolution(),

  playerPosition: {
    x: 2,
    y: 1
  },

  startPosition: {
    x: 2,
    y: 1
  },

  endPosition: {
    x: 2,
    y: 3
  },

  // calculate the new position "if" the player moves in the direction
  getMovePosition: function(direction) {
    var x = this.playerPosition.x;
    var y = this.playerPosition.y;

    // move the player to the new position
    if(!isNaN(direction.right)) {
      x -= direction.right;
    } else if(!isNaN(direction.left)) {
      x += direction.left;
    } else if(!isNaN(direction.forward)) {
      y += direction.forward;
    } else if(!isNaN(direction.back)) {
      y -= direction.back;
    } 

    return {
      x: x,
      y: y
    };
  },

  movePlayer: function(direction) {
    // reduce the tile that we currently are on
    var x = this.playerPosition.x;
    var y = this.playerPosition.y;

    this.reduceTile();
    
    // var p = getMovePosition(direction);

    // this.playerPosition.x = p.x;
    // this.playerPosition.y = p.y;


    // move the player to the new position
    if(!isNaN(direction.right)) {
      this.playerPosition.x -= direction.right;
    } else if(!isNaN(direction.left)) {
      this.playerPosition.x += direction.left;
    } else if(!isNaN(direction.forward)) {
      this.playerPosition.y += direction.forward;
    } else if(!isNaN(direction.back)) {
      this.playerPosition.y -= direction.back;
    }

    this.solution.movePlayer(direction);

    return {
      x: this.playerPosition.x,
      y: this.playerPosition.y
    };
  },

  getItem: function(x, y) {
    var height = this.canvas[y*this.sizeY + x];

    return {
      height: height != undefined ? height : 0,
      isEnd : (x == this.endPosition.x && y == this.endPosition.y),
      isPlayer : (x == this.playerPosition.x && y == this.playerPosition.y)
    };
  },

  resetLevel: function() {
    this.canvas = this.level.canvas.slice();

    this.endPosition.x = this.level.endPosition.x;
    this.endPosition.y = this.level.endPosition.y;

    this.startPosition.x = this.level.startPosition.x;
    this.startPosition.y = this.level.startPosition.y;

    this.playerPosition.x = this.startPosition.x;
    this.playerPosition.y = this.startPosition.y;

    this.solution = this.level.solution;
    this.solution.resetMoves();
  },

  nextLevel: function() {
    this.levelNum++;

    this.level = generateLevel({level : this.levelNum});
    this.resetLevel();    
  },

  gotoLevel: function(lvl) {
    this.levelNum = lvl;

    this.level = generateLevel({level : this.levelNum});
    this.resetLevel();    
  },

  generateCanvas: function() {
    this.level = generateLevel({level : this.levelNum});
    this.resetLevel();
  },

  getPlayerPosition: function() {
    return this.playerPosition;
  },

  reduceTile: function() {
    var x = this.playerPosition.x;
    var y = this.playerPosition.y;
    if(this.canvas[y*this.sizeY + x] > 0) {
      this.canvas[y*this.sizeY + x]--;
    }
  },

  hasPlayerLost: function() {
    if(this.getItem(this.playerPosition.x, this.playerPosition.y).height <= 0) {
      return true;
    }
    if(this.getItem(this.endPosition.x, this.endPosition.y).height <= 0) {
      return true;
    }
  },

  hasPlayerWon: function() {
    if(this.playerPosition.x == this.endPosition.x && this.playerPosition.y == this.endPosition.y) {
      
      var s = 0;
      for(var i=0; i < this.canvas.length; i++) {
        s += this.canvas[i];
      }

      if(s == 1) {
        return true;
      }
    }
  }
};

function LevelSolution() {
  this.curMove = 0;
  this.derailed = false;
  this.moves = new Array();
}

LevelSolution.prototype = {
  moves: new Array(),
  curMove : 0,
  curPosition : {
    x:0,
    y:0
  },

  // knows if player has derailed from the solution path
  derailed: false,

  resetMoves : function() {
    this.curMove = 0;
    this.derailed = false;
  },

  addMove: function(direction, distance) {
    this.moves.push({
      dir : direction,
      dist: distance
    });
  },

  getNextMove: function() {
    if(this.derailed) {
      return null;
    }

    return this.moves[this.moves.length - 1 - this.curMove];
  },

  getNextPosition: function(position) {
    if(this.derailed) {
      return null;
    }

    var move = this.getNextMove();
    if(move == null || move == undefined) {
      return null;
    }

    if(position == undefined) {
      position = game.playerPosition;
    }

    var x = game.playerPosition.x;
    var y = game.playerPosition.y;

    if(move.dir == "left") {
      x += move.dist;
    } else if(move.dir == "right") {
      x -= move.dist;
    } else if(move.dir == "back") {
      y -= move.dist;
    } else {
      y += move.dist;
    }

    return {x:x,y:y};
  },

  // checks if the user is following the solutions and moves to the next one.
  movePlayer: function(direction) {
    if(this.derailed) {
      return;
    }
    // var nextPos = this.getNextPosition();
    // var playerPos = game.playerPosition;
    // if(nextPos.x == playerPos.x && nextPos.y == playerPos.y) {
    var nextMove = this.getNextMove();
    if(direction[nextMove.dir] == nextMove.dist) {
      this.curMove++;
    } else {
      this.derailed = true;
    }
  }
}

function generateLevel(options) {
    var steps = 5;
    var maxJumps = 2;
    var maxHeight = 3;
    var verticalWeight = 0.1;
    var sizeX = 10;
    var sizeY = 10;
    var level = 1;

    var solution = new LevelSolution();

    if(options != undefined) {
      if(options.steps != undefined) {
        steps = options.steps;
      }
      if(options.maxJumps != undefined) {
        maxJumps = options.maxJumps;
      }
      if(options.maxHeight != undefined) {
        maxHeight = options.maxHeight;
      }

      if(options.level != undefined) {
        level = options.level;
        steps += Math.floor(level/2);
      }
    }

    //console.log("steps: " + steps);

    var canvas = [];
    for (var i = 0; i < 100; i++) {
      canvas[i] = 0;
    }

    var endX = Math.floor(Math.random()*10);
    var endY = Math.floor(Math.random()*10);

    var posX = endX;
    var posY = endY;

    canvas[coordsToIndex(posX, posY, sizeY)] = 1;

    // avoid getting in loophole when all the tiles around have maxHeight
    var loop = 0;
    var i = 0;
    var direction;
    while(i < steps) {
      
      if(++loop == 10) {
        console.log("reached loophole - restarting");
        return generateLevel(options);
      }

      dir = Math.floor(Math.random()*4);
      step = 1 + Math.floor(Math.random()*maxJumps);
      
      nPosX = posX;
      nPosY = posY;

      if(dir == 0) {
        if(posX - step < 0) {
          continue;
        }
        nPosX -= step;
        direction = "left";
      } else if(dir == 1) {
        if(posX + step > 9) {
          continue;
        }
        nPosX += step;
        direction = "right";
      } else if(dir == 2) {
        if(posY + step > 9) {
          continue;
        }
        nPosY += step;
        direction = "back";
      } else {
        if(posY - step < 0) {
          continue;
        }
        nPosY -= step;
        direction = "forward";
      }

      var h = canvas[coordsToIndex(nPosX, nPosY, sizeY)];
      // console.log("("+nPosX+","+nPosY+") - height: " + h);

      if(h == maxHeight) {
        continue;
      } else if(h !== 0) {
        // count in the vertical weight
        var p = Math.pow(verticalWeight,h);
        var r = Math.random();
        // console.log("chance: " + p + " - random : " + r);
        if(p < r) {
          // console.log("dropped");
          continue;
        }
      }
      
      solution.addMove(direction, step);

      canvas[coordsToIndex(nPosX, nPosY, sizeY)]++;
      i++;
      loop = 0;
      posX = nPosX;
      posY = nPosY;
    }

    var lowerX = sizeX;
    var lowerY = sizeX;
    //bring the tiles closer to 0,0
    for(var i=0; i < canvas.length; i++) {
      if(canvas[i] != 0) {
        coords = indexToCoords(i, sizeY);
        if(coords.x < lowerX) {
          lowerX = coords.x;
        }
        if(coords.y < lowerY) {
          lowerY = coords.y;
        }
      }
    }
    // console.log(lowerX + ":" + lowerY);
    var lindx = coordsToIndex(lowerX, lowerY, sizeY);

    for(var i=0; i < canvas.length; i++) {
      canvas[i] = canvas[i+lindx] != undefined ? canvas[i+lindx] : 0;
    }

    posX -= lowerX;
    posY -= lowerY;
    endX -= lowerX;
    endY -= lowerY;

    return {
      options: {
        lnum: level,
        steps: steps,
        maxHeight: maxHeight
      },

      canvas: canvas,

      startPosition: {
        x: posX,
        y: posY
      },

      endPosition: {
        x: endX,
        y: endY
      },

      solution: solution
    }
}

function coordsToIndex(x, y, sizeY) {
  return y*sizeY + x;
}

function indexToCoords(i, sizeY) {
  return {
    x: i%sizeY,
    y: Math.floor(i/sizeY)
  };
}