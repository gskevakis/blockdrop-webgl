
var miniMap;

function initMiniMap() {
  var canvas = document.getElementById("miniMap");
  miniMap = canvas.getContext("2d");

  miniMap.roundRect = function(x, y, w, h, radius, strokeWidth, strokeStyle) {
    var r = x + w;
    var b = y + h;
    this.beginPath();
    if(strokeStyle == undefined ) {
      this.strokeStyle = "#444444";    
    } else {
      this.strokeStyle = strokeStyle;
    }
    this.fillStyle = "#444444";
    this.lineWidth=strokeWidth;
    this.moveTo(x+radius, y);
    this.lineTo(r-radius, y);
    this.quadraticCurveTo(r, y, r, y+radius);
    this.lineTo(r, y+h-radius);
    this.quadraticCurveTo(r, b, r-radius, b);
    this.lineTo(x+radius, b);
    this.quadraticCurveTo(x, b, x, b-radius);
    this.lineTo(x, y+radius);
    this.quadraticCurveTo(x, y, x+radius, y);
    this.stroke();
    this.fill();
  }

  drawMiniMap();
}

function drawMiniMap() {
  miniMap.clearRect ( 0 , 0 , 120 , 120 );
  miniMap.save();
  miniMap.translate(114,114);

  var nextPosition = game.solution.getNextPosition();
  
  for(var i=0; i < game.sizeY; i++) {
    // console.log("here");
    miniMap.save();
    for(var j=0; j < game.sizeX; j++) {
      var tile = game.getItem(i,j);
      // console.log("here2");
      if(tile.height > 0) {
        // console.log(tile);
        
        if(helpMe && nextPosition!=null && nextPosition.x == i && nextPosition.y == j) {
          miniMap.translate(1, 1);
          miniMap.roundRect(-5, -5, 8, 8, 2, 4, "yellow");
          miniMap.translate(-1, -1);
        } else if(tile.isEnd) {
          miniMap.translate(1, 1);
          miniMap.roundRect(-5, -5, 8, 8, 2, 4, "red");
          miniMap.translate(-1, -1);
        } else if(tile.isPlayer){
          miniMap.translate(1, 1);
          miniMap.roundRect(-5, -5, 8, 8, 2, 4, "#42F56C");
          miniMap.translate(-1, -1);
        } else {
          miniMap.roundRect(-5, -5, 10, 10, 2, 2);
        }

        if(tile.height > 1) {
          miniMap.font = '9pt Calibri';
          miniMap.textAlign = 'center';
          miniMap.fillStyle = 'white';
          miniMap.fillText(tile.height, 0, 4);
        }
      }
      miniMap.translate(0, -12);
    }
    miniMap.restore();
    miniMap.translate(-12, 0);
  }
  miniMap.restore();
}