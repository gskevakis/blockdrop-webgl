function Scene(width, height) {
  this.width = width;
  this.height = height;

  this.objects = [];
}


function ObjectVectorData(itemSize) {
  this.itemSize = itemSize;
  this.numItems = 0;
  this._vector = [];
  this.buffer = null;
}

ObjectVectorData.prototype = {
  set vector(data) {
    this._vector = data;
    this.numItems = data.length / this.itemSize;

    // setup the buffer
    this.buffer = gl.createBuffer();

    if (this.itemSize == 1) {
      gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.buffer);
      gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(this._vector), gl.STATIC_DRAW);
    } else {
      gl.bindBuffer(gl.ARRAY_BUFFER, this.buffer);
      gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(this._vector), gl.STATIC_DRAW);
    }
  }, get vector() {
    return this._vector;
  }
};


function SceneObject() {
  this._twoD = false;
  this._vertices = null;
  this._color = [];
  this._texture = null;
  this._normals = null;
  this._vertexIndices = null;
}

SceneObject.prototype = {
  set vertices(data) {
    this._vertices = new ObjectVectorData(3);
    this._vertices.vector = data;
  }, set color(data) {
    this._color = new ObjectVectorData(4);
    this._color.vector = data;
  }, set texture(data) {
    this._texture = new ObjectVectorData(2);
    this._texture.vector = data;
  }, set vertexIndices(data) {
    this._vertexIndices = new ObjectVectorData(1);
    this._vertexIndices.vector = data;
  }, set normals(data) {
    this._normals = new ObjectVectorData(3);
    this._normals.vector = data;
  }, get vertices() {
    return this._vertices;
  }, get color() {
    return this._color;
  }, get texture() {
    return this._texture;
  }, get vertexIndices() {
    return this._vertexIndices;
  }, get normals() {
    return this._normals;
  }, draw: function(colorOverride) {
    var color = this._color;

    if (colorOverride !== undefined) {
      color = new ObjectVectorData(4);
      color.vector = this.getColorVector(colorOverride.r, colorOverride.g, colorOverride.b, colorOverride.a);
    }

    gl.bindBuffer(gl.ARRAY_BUFFER, this.vertices.buffer);
    gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, this.vertices.itemSize, gl.FLOAT, false, 0, 0);

    // we bind the buffer for the object colors
    // gl.bindBuffer(gl.ARRAY_BUFFER, color.buffer);
    // gl.vertexAttribPointer(shaderProgram.vertexColorAttribute, color.itemSize, gl.FLOAT, false, 0, 0);

    // set the white texture if it's null
    // bind texture
    if(this.texture !== null) {
      gl.bindBuffer(gl.ARRAY_BUFFER, this.texture.buffer);
      gl.vertexAttribPointer(shaderProgram.textureCoordAttribute, this.texture.itemSize, gl.FLOAT, false, 0, 0);
      // gl.activeTexture(gl.TEXTURE0);
      // gl.bindTexture(gl.TEXTURE_2D, neheTexture);
      // gl.uniform1i(shaderProgram.samplerUniform, 0);
    }

    if(this.normals !== null) {
      gl.bindBuffer(gl.ARRAY_BUFFER, this.normals.buffer);
      gl.vertexAttribPointer(shaderProgram.vertexNormalAttribute, this.normals.itemSize, gl.FLOAT, false, 0, 0);
    }

    if (!this._twoD && this._vertexIndices !== null) {
      //we bind the buffer for the 3d object vertex indices
      gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.vertexIndices.buffer);
    }

    //we update the uniforms for the shaders
    setMatrixUniforms();

    if (!this._twoD) {
      if(this._vertexIndices !== null) {
        //we call the Draw Call of WebGL to draw the 3d object
        gl.drawElements(gl.TRIANGLES, this.vertexIndices.numItems, gl.UNSIGNED_SHORT, 0);
      } else {
        gl.drawArrays(gl.TRIANGLES, 0, this.vertices.numItems);
      }
    } else {
      //we call the Draw Call of WebGL to draw the d object
      gl.drawArrays(gl.TRIANGLE_STRIP, 0, this.vertices.numItems);
    }
  },
  getColorVector: function(r, g, b, a) {
    colorTable = [];
    for (var i = 0; i < this.vertices.numItems; i++) {
      colorTable = colorTable.concat([r, g, b, a]);
    }
    return colorTable;
  }
};

function Scene3dObject() {
    var that = new SceneObject();
    return that;
}

function Scene2dObject() {
    var that = new SceneObject();
    that._twoD = true;
    return that;
}



// holding the textures, handling their initialization and drawing
function TextureManager() {
}

TextureManager.prototype = {
  getTexture: function(name) {
    return this.textures[name];
  },

  // if the texture exists, updates the image. if not, creates a new texture.
  setTexture: function(name, url) {
    var texture = gl.createTexture();
    this[name] = texture;

    texture.image = new Image();
    texture.image.onload = function () {
        gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
        gl.bindTexture(gl.TEXTURE_2D, texture);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, texture.image);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_NEAREST);
        gl.generateMipmap(gl.TEXTURE_2D);

        gl.bindTexture(gl.TEXTURE_2D, null);


    }
    texture.image.src = url;

    texture.set = function() {
      gl.activeTexture(gl.TEXTURE0);
      gl.bindTexture(gl.TEXTURE_2D, this);
      gl.uniform1i(shaderProgram.samplerUniform, 0);
    }

    return texture;
  }
};